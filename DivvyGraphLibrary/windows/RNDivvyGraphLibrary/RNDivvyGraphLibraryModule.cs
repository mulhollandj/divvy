using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Divvy.Graph.Library.RNDivvyGraphLibrary
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNDivvyGraphLibraryModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNDivvyGraphLibraryModule"/>.
        /// </summary>
        internal RNDivvyGraphLibraryModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNDivvyGraphLibrary";
            }
        }
    }
}
