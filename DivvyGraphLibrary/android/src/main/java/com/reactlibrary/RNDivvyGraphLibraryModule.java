
package com.reactlibrary;

import android.widget.Toast;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;

public class RNDivvyGraphLibraryModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;

  public RNDivvyGraphLibraryModule(ReactApplicationContext reactContext) {
    super(reactContext);
    this.reactContext = reactContext;
  }

  @ReactMethod
  public void saySpecial() {
      Toast.makeText(getReactApplicationContext(), "Special", Toast.LENGTH_LONG);
  }

  @Override
  public String getName() {
    return "RNDivvyGraphLibrary";
  }
}