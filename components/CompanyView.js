import React, {Component} from 'react'
import {AppRegistry, Text, View, Button, Navigation}  from 'react-native'
import DetailsScreen from '../screens/DetailsScreen';
import { createStackNavigator} from 'react-navigation';
const DetailsStack = createStackNavigator({
  Details: DetailsScreen,
});

export class CompanyView extends Component {
  static navigationOptions = {
    title: "Welcome",
  };
  render() {
    let company = {name: this.props.name, address: this.props.address}

    return (
        <Button style={this.props.style} title={this.props.name} onPress={() => this.props.navigation.navigate('Details', {company})}></Button>
    )
  }
}
